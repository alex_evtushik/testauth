package test.testauthfacebookapi;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;

import com.facebook.widget.LoginButton;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import io.fabric.sdk.android.Fabric;


public class MainActivity extends ActionBarActivity {

    private static final String TWITTER_KEY = "n20Dpcx1Lfy7SUFfyt6AY25Cy";
    private static final String TWITTER_SECRET = "9dRC4rIZdWFVlUHrTWA3kPzL6x7cKgurPsMaE84dBrURrntJvZ";

    public static final String TAG = MainActivity.class.getSimpleName();
    private UiLifecycleHelper uiHelper;
    private LoginButton authFacebook;
    private TwitterLoginButton authTwitter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        authFacebook = (LoginButton) findViewById(R.id.auth_facebook);
        authTwitter = (TwitterLoginButton) findViewById(R.id.auth_twitter);
        authTwitter.setText("Log in");
        authTwitter.setCallback(twitterSessionCallback);

    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState sessionState, Exception e) {
            onSessionStateChange(session, sessionState, e);
        }
    };

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {
            Log.d(TAG, "Logged in Facebook");
            Log.d(TAG, "Access token = " + session.getAccessToken());
            Request.newMeRequest(session, userCallback).executeAsync();
        } else if (state.isClosed()) {
            Log.d(TAG, "Logged out Facebook");
        }
    }

    private Request.GraphUserCallback userCallback = new Request.GraphUserCallback() {
        @Override
        public void onCompleted(GraphUser graphUser, Response response) {
            if (graphUser != null) {
                String res = graphUser.getInnerJSONObject().toString();
                Log.d(TAG, "res = " + res);

                Log.d(TAG, "id = " + graphUser.getId());
                Log.d(TAG, "name = " + graphUser.getName());
                Toast.makeText(MainActivity.this, "Hello " + graphUser.getName(), Toast.LENGTH_SHORT).show();
            }
        }
    };

    private Callback<TwitterSession> twitterSessionCallback = new Callback<TwitterSession>() {

        @Override
        public void success(Result<TwitterSession> result) {
            Log.d(TAG, "token = " + result.data.getAuthToken().token);
            Log.d(TAG, "secret = " + result.data.getAuthToken().secret);
            Log.d(TAG, "name = " + result.data.getUserName());
            Toast.makeText(MainActivity.this, "Hello " + result.data.getUserName(), Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Logged in Twitter");
        }

        @Override
        public void failure(TwitterException exception) {
            Log.d(TAG, "exception = " + exception);
        }
    };

    public void logoutTwitter(View v) {
        Twitter.getSessionManager().clearActiveSession();
        Log.d(TAG, "Logged out Twitter");
    }

    public void logoutFacebook(View v) {
        Session session = Session.getActiveSession();
        if (session != null & !session.isClosed()) {
               session.closeAndClearTokenInformation();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
        authTwitter.onActivityResult(requestCode, resultCode, data);
     }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            Log.d(TAG, "session id opened, ");
        }

        TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
        if (twitterSession != null) {
            Toast.makeText(MainActivity.this, "Hello " + twitterSession.getUserName(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        uiHelper.onSaveInstanceState(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
